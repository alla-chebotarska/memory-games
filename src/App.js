import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from './components/Home/Home';
import FindTheSame from './components/FindTheSame/FindTheSame';
import TranslateToEnglish from './components/TranslateToEnglish/TranslateToEnglish';
import TranslateToRU from './components/TranslateToRU/TranslateToRU';
import ToDoList from './components/ToDoList/ToDoList';

function App() {
  return (
    <div className="App">
     <Router>
       <Route exact path='/' component={Home}/>
       <Route exact path='/FindTheSameS' render={(props) => <FindTheSame {...props} height={4}/>} />
       <Route exact path='/FindTheSameM' render={(props) => <FindTheSame {...props} height={6}/>} />
       <Route exact path='/translate' component={TranslateToEnglish} />
       <Route exact path='/translateToRU' component={TranslateToRU} />
       <Route exact path='/todolist' component={ToDoList} />
     </Router>
    </div>
  );
}

export default App;
