import { Box, Button, Container, Grid } from '@material-ui/core';
import React, { Component } from 'react';
import nextPath from '../../services/LocationService';
import './FindTheSame.css';


export default class FindTheSame extends Component {

    constructor(props) {
        super(props);
        this.state = {
            cells: this.generateValueForCells(),
            openedCount: 0
        }
    }

    /**
     * Shuffles array in place. ES6 version
     * @param {Array} a items An array containing the items.
     */
    shuffle(a) {
        for (let i = a.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [a[i], a[j]] = [a[j], a[i]];
        }
        return a;
    }

    generateValueForCells = () => {
        let rangeOfValue = (4 * this.props.height) / 2;
        let valueOfCells = [];
        for (let i = 1; i <= rangeOfValue; i++) {
            valueOfCells.push({ value: i, isOpen: false, isGassed: false });
            valueOfCells.push({ value: i, isOpen: false, isGassed: false });
        }
        this.shuffle(valueOfCells);
        return valueOfCells;
    }

    onCellClick = (index) => {
        if (this.state.openedCount === 2) {
            return;
        }
        let cellsArray = this.state.cells;
        cellsArray[index].isOpen = true;
        let opened = this.state.openedCount + 1;
        this.setState({
            cells: cellsArray,
            openedCount: opened,
        })
        if (opened === 2) {
            this.isValueTheSame(index)
        }
    }



    isValueTheSame = (index) => {
        let cellsArray = this.state.cells;
        cellsArray.forEach((element, i) => {
            if (index !== i && element.isOpen) {
                if (element.value === cellsArray[index].value) {
                    element.isGassed = true;
                    cellsArray[index].isGassed = true;
                    element.isOpen = true;
                    cellsArray[index].isOpen = true;
                } else {
                    setTimeout(() => {
                        element.isOpen = false
                        cellsArray[index].isOpen = false;
                        this.setState({
                            cells: cellsArray,
                        })
                    }, 500)
                }
            }
        })
        this.setState({
            cells: cellsArray,
            openedCount: 0,
        })
    }

    startNewGame = () => {
        this.setState({
            cells: this.generateValueForCells(),
            openedCount: 0,
        })
    }


    render() {
        return (
            <Container maxWidth="xs">
                <Grid container spacing={2}>
                    <Grid item xs={4}>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={() => nextPath(this.props, '/')}>
                            Go Home
                        </Button>
                    </Grid>
                    <Grid item xs={4}>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={this.startNewGame}>
                            New Game
                        </Button>
                    </Grid>
                </Grid>
                <Grid container spacing={1}>
                    {
                        this.state.cells
                            .map((cell, index) => (<Grid item xs={12} sm={3}>
                                <Box
                                    className="box"
                                    bgcolor={cell.isGassed ? "success.main" : "primary.main"}
                                    color="primary.contrastText"
                                    p={2}
                                    onClick={() => this.onCellClick(index)}>
                                    {cell.isOpen || cell.isGassed ? <span>{cell.value}</span> : '*'}
                                </Box>
                            </Grid>)
                            )
                    }

                </Grid>
            </Container>
        )
    }
}
