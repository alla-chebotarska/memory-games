import React, { Component } from 'react';
import { Container, Box, Grid } from '@material-ui/core';
import './TranslateToEnglish.css';

export default class TranslateToEnglish extends Component {

    constructor() {
        super();
        this.state = {
            words: ['car', 'house', 'night', 'flower'],
            translation: ['машина', 'дом', 'ночь', 'цветок'],
            position: 0,
        }
    }

    shuffle(a) {
        for (let i = a.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [a[i], a[j]] = [a[j], a[i]];
        }
        return a;
    }

    onTranslateWordClick = (word) => {
        if (this.state.translation.indexOf(word) === this.state.position) {
            if (this.state.position < this.state.translation.length - 1) {
                this.setState({
                    position: this.state.position + 1,
                })
            } else {
                this.setState({
                    position: 0,
                })
            }
        } else {
            alert("Wrong");
        }
    }

    render() {
        return (
            <div>
                <Container maxWidth="xs">
                    <h3 className='word-to-translate'>
                        <span>{this.state.words[this.state.position]}</span>
                    </h3>
                    <Grid container spacing={1}>
                        {
                            this.shuffle(this.state.translation
                                .map((item) =>
                                    <Grid item xs={3}
                                        key={item}>
                                        <Box

                                            bgcolor="primary.main"
                                            color="primary.contrastText"
                                            className='translate box'
                                            p={2}
                                            onClick={() => this.onTranslateWordClick(item)}>
                                            <span>{item}</span>
                                        </Box>
                                    </Grid>
                                )
                            )
                        }

                    </Grid>
                </Container>
            </div >
        )
    }
}
