import React, { Component } from 'react';
import { Container, Grid } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import nextPath from '../../services/LocationService';


export default class Home extends Component {
    render() {
        return (
            <Container maxWidth="sm">
                <h1>Choose the game you like</h1>
                <Grid container spacing={2}>
                    <Grid item xs={4}>
                        <Button
                            variant='contained'
                            color="primary"
                            onClick={() => nextPath(this.props, '/FindTheSameS')}>
                            Find The Same S
                        </Button>
                    </Grid>
                    <Grid item xs={4}>
                        <Button
                            variant='contained'
                            color="primary"
                            onClick={() => nextPath(this.props, '/FindTheSameM')}>
                            Find The Same M
                        </Button>
                    </Grid>
                    <Grid item xs={4}>
                        <Button
                            variant='contained'
                            color="primary"
                            onClick={() => nextPath(this.props, '/Translate')}>
                            Translate to EN
                        </Button>
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                <Grid item xs={4}>
                        <Button
                            variant='contained'
                            color="primary"
                            onClick={() => nextPath(this.props, '/translateToRu')}>
                            Translate to RU
                        </Button>
                    </Grid>
                    <Grid item xs={4}>
                        <Button
                            variant='contained'
                            color="primary"
                            onClick={() => nextPath(this.props, '/todolist')}>
                            To Do List
                        </Button>
                    </Grid>
                </Grid>
            </Container >
        )
    }
}
