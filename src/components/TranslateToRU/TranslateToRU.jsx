import React, { Component } from 'react';
import { Container, Box, Grid } from '@material-ui/core';

export default class TranslateToRU extends Component {

    constructor() {
        super();
        this.state = {
            wordsArr: [{ word: 'flower', translation: 'цветок' }, { word: 'car', translation: 'машина' }, { word: 'house', translation: 'дом' }, { word: 'night', translation: 'ночь' }],
            position: 0,
        }
    }

    onTranslationClick = (item) => {
        console.log(this.state.wordsArr.indexOf(item));
        if(this.state.wordsArr.indexOf(item) === this.state.position){
            if(this.state.position < this.state.wordsArr.length - 1 ){
                this.setState({
                position: this.state.position + 1,
            })
            }else{
                this.setState({
                    position: 0,
                })
            }
            
        }else{
            alert("this is wrong translation");
        }
    }

    render() {
        return (
            <div>
                <Container maxWidth="sm">
                    <h3>
                        <span>{this.state.wordsArr[this.state.position].word}</span>
                    </h3>
                    <Grid container spacing={1}>
                        {
                            this.state.wordsArr
                                .map((item) =>
                                    <Grid item xs={3}
                                        key={item.word}>
                                        <Box

                                            bgcolor="primary.main"
                                            color="primary.contrastText"
                                            className='translate box'
                                            p={2}
                                            onClick={() => this.onTranslationClick(item)}
                                        >
                                            <span>{item.translation}</span>
                                        </Box>
                                    </Grid>
                                )
                        }

                    </Grid>
                </Container>
            </div>
        )
    }
}
