import React, { Component } from 'react'

export default class ToDoList extends Component {

    constructor() {
        super();
        this.state = {
            list: ['first', 'second'],
            input: '',
            filter: '',
        }
    }

    onInputChange = (event) => {
        this.setState({
            input: event.target.value,
        })
    }

    onAddBtnClick = () => {
        let newItem = this.state.input;
        let listItem = this.state.list;
        listItem.push(newItem);
        this.setState({
            list: listItem,
            input: '',
        })
    }

    Filter = (event) => {
        this.setState({
            filter: event.target.value,
        })
    }

    render() {
        return (
            <div>
                <ul>
                    {
                        this.state.list
                        .filter((item) => item.toLocaleUpperCase().includes(this.state.filter.toLocaleUpperCase()))
                        .map((item) => <li>{item}</li>)
                    }
                </ul>
                <input
                    value={this.state.input}
                    onChange={this.onInputChange}></input>
                <button
                    onClick={this.onAddBtnClick}>
                    Add
                </button>
                <div>
                    <input
                        value={this.state.filter}
                        onChange={this.Filter}>
                    </input>
                </div>
            </div>
        )
    }
}
